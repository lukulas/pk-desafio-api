### Iniciando o projeto

#### clonando o projeto
`git clone https://github.com/lucas-sobierajski-oliveira/pk-desafio-api`

`cd pk-desafio-api`

#### instalando as dependência
`yarn ou npm install`

#### iniciando o servidor
`yarn dev:server`
