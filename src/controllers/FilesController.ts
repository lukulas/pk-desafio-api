import { Request, Response } from 'express';
import Promise from 'bluebird';
import { join } from 'path';
import { v4 } from 'uuid';
import htmlpdf from 'html-pdf';

const pdf = Promise.promisifyAll(htmlpdf);

interface ILocal {
  nome_fantasia: string;
  cidade: string;
  bairro: string;
  endereco: string;
  distancia: string;
  telefone: string;
}

interface ILocalities {
  localities: ILocal[];
}

class FilesController {
  async store(request: Request, response: Response) {
    const fileName = `${v4()}.pdf`;

    if (request.body.localities.length < 1) {
      return response.status(400).json({ error: 'O array está vazio' });
    }

    const { localities }: ILocalities = request.body;

    const html = `
    <table>
      <thead>
        <tr>
          <th>Estabelecimento</th>
          <th>Cidade</th>
          <th>Bairro</th>
          <th>Rua</th>
          <th>distancia</th>
          <th>Telefone</th>
        </tr>
      </thead>
      <tbody>
      ${localities.map(local => {
        return `
          <tr>
          <td>${local.nome_fantasia}</td>
          <td>${local.cidade}</td>
          <td>${local.bairro}</td>
          <td>${local.endereco}</td>
          <td>${local.distancia}</td>
          <td>${local.telefone}</td>
        </tr>
          `;
      })}

    </table>
    `;

    await pdf.createAsync(html, {
      format: 'A4',
      filename: join(__dirname, '..', 'docs', fileName),
    });

    return response.send(fileName);
  }
}

export default new FilesController();
