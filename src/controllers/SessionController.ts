import { Request, Response } from 'express';
import { sign } from 'jsonwebtoken';

import api from '../services/apiPegaki';
import tokenConfig from '../config/token';
import AppError from '../errors/AppError';

class SessionController {
  async store(request: Request, response: Response): Promise<Response> {
    const { email, client_secret } = request.body;

    try {
      const responseData = await api.post('/authentication', {
        email,
        client_secret,
      });

      const { id_token } = responseData.data;

      return response.json({
        email,
        token: sign({ id_token }, tokenConfig.secret, {
          expiresIn: tokenConfig.expires,
        }),
      });
    } catch (error) {
      throw new AppError(error.response.data.message, error.status);
    }
  }
}

export default new SessionController();
