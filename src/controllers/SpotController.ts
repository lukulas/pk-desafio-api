import { Response, Request } from 'express';
import api from '../services/apiPegaki';

import AppError from '../errors/AppError';

class SpotController {
  async index(request: Request, response: Response): Promise<Response> {
    const { cep } = request.params;
    const { id_token } = request;

    try {
      const { data } = await api.get(
        `https://api.pegaki.com.br/pontos/${cep}`,
        {
          headers: {
            Authorization: id_token,
          },
        },
      );

      return response.send(data.results);
    } catch (error) {
      throw new AppError(error.response.data.message, error.status);
    }
  }
}

export default new SpotController();
