import { Response, Request, NextFunction } from 'express';
import { verify } from 'jsonwebtoken';

import AppError from '../errors/AppError';

import tokenConfig from '../config/token';

interface ITokenPayload {
  iat: number;
  exp: number;
  sub: string;
  id_token: string;
}

export default function authMiddleware(
  request: Request,
  response: Response,
  next: NextFunction,
): void {
  const authHeader = request.headers.authorization;

  if (!authHeader) {
    throw new AppError('Token inexistente.', 401);
  }

  const [, token] = authHeader.split(' ');
  try {
    const decoded = verify(token, tokenConfig.secret);
    const { id_token } = decoded as ITokenPayload;

    request.id_token = id_token;
    return next();
  } catch (error) {
    throw new AppError('Token inválido', 401);
  }
}
