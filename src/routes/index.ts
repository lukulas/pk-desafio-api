import express, { Router } from 'express';
import { join } from 'path';

import authMiddleware from '../middleware/authMiddleware';
import errorMiddleware from '../middleware/errorMiddleware';

import SessionController from '../controllers/SessionController';
import SpotController from '../controllers/SpotController';
import FilesController from '../controllers/FilesController';

const routes = Router();

routes.post('/files', authMiddleware, FilesController.store);

routes.use('/files', express.static(join(__dirname, '..', 'docs')));

routes.post('/session', SessionController.store);

routes.get('/cep/:cep', authMiddleware, SpotController.index);

routes.use(errorMiddleware);

export default routes;
