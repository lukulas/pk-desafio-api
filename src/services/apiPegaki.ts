import axios from 'axios';

const api = axios.create({ baseURL: 'https://api.pegaki.com.br' });

export default api;
